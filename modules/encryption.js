/**
 *  <p>文件名称：AosSecurityEncode.js</p>
 *  <p>文件描述：</p>
 *  <p>内容摘要：</p>
 *  <p>使用说明：</p>
 *  <p>完成日期：2013-3-18</p>
 *  <p>@version 1.0</p>
 *  <p>作    者：yangxin</p>
 *  <p>版权所有: 版权所有(C)2009-2010</p>
 *  <p>公    司：深圳市中兴通讯股份有限公司</p>
 */
var AosSecurityEncode = {
	encodeChars : "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/",
	decodeChars : [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
			-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
			-1, 62, -1, -1, -1, 63, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, -1, -1, -1, -1, -1, -1,
			-1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22,
			23, 24, 25, -1, -1, -1, -1, -1, -1, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38,
			39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, -1, -1, -1, -1, -1 ],
	aosEncode : function(str) {
		var out, i, len;
		var c1, c2, c3;
		len = str.length;
		i = 0;
		out = "";
		while ( i < len ) {
			c1 = str.charCodeAt( i++ ) & 0xff;
			if ( i == len ) {
				out += AosSecurityEncode.encodeChars.charAt( c1 >> 2 );
				out += AosSecurityEncode.encodeChars.charAt( (c1 & 0x3) << 4 );
				out += "==";
				break;
			}

			c2 = str.charCodeAt( i++ );

			if ( i == len ) {
				out += AosSecurityEncode.encodeChars.charAt( c1 >> 2 );
				out +=
						AosSecurityEncode.encodeChars.charAt( ((c1 & 0x3) << 4)
																| ((c2 & 0xF0) >> 4) );
				out += AosSecurityEncode.encodeChars.charAt( (c2 & 0xF) << 2 );
				out += "=";
				break;
			}
			c3 = str.charCodeAt( i++ );
			out += AosSecurityEncode.encodeChars.charAt( c1 >> 2 );
			out += AosSecurityEncode.encodeChars.charAt( ((c1 & 0x3) << 4) | ((c2 & 0xF0) >> 4) );
			out += AosSecurityEncode.encodeChars.charAt( ((c2 & 0xF) << 2) | ((c3 & 0xC0) >> 6) );
			out += AosSecurityEncode.encodeChars.charAt( c3 & 0x3F );
		}
		return out;
	},
	aosDecode : function(str) {
		var c1, c2, c3, c4;
		var i, len, out;
		len = str.length;
		i = 0;
		out = "";
		while ( i < len ) {
			do {
				c1 = AosSecurityEncode.decodeChars[str.charCodeAt( i++ ) & 0xff];
			} while ( i < len && c1 == -1 );

			if ( c1 == -1 )
				break;

			do {
				c2 = AosSecurityEncode.decodeChars[str.charCodeAt( i++ ) & 0xff];
			} while ( i < len && c2 == -1 );

			if ( c2 == -1 )
				break;

			out += String.fromCharCode( (c1 << 2) | ((c2 & 0x30) >> 4) );

			do {
				c3 = str.charCodeAt( i++ ) & 0xff;

				if ( c3 == 61 )
					return out;

				c3 = AosSecurityEncode.decodeChars[c3];
			} while ( i < len && c3 == -1 );

			if ( c3 == -1 )
				break;

			out += String.fromCharCode( ((c2 & 0XF) << 4) | ((c3 & 0x3C) >> 2) );

			do {
				c4 = str.charCodeAt( i++ ) & 0xff;

				if ( c4 == 61 )
					return out;

				c4 = AosSecurityEncode.decodeChars[c4];
			} while ( i < len && c4 == -1 );

			if ( c4 == -1 )
				break;

			out += String.fromCharCode( ((c3 & 0x03) << 6) | c4 );
		}

		return out;
	}
};
module.exports = AosSecurityEncode;
// Object.ClassFactory.defineClass( "AosSecurityEncode" );
// isc.AosSecurityEncode.addClassProperties( {
// 	encodeChars : "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/",
// 	decodeChars : [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
// 			-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
// 			-1, 62, -1, -1, -1, 63, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, -1, -1, -1, -1, -1, -1,
// 			-1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22,
// 			23, 24, 25, -1, -1, -1, -1, -1, -1, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38,
// 			39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, -1, -1, -1, -1, -1 ]
// } );
// isc.AosSecurityEncode.addClassMethods( {
// 	aosEncode : function(str) {
// 		var out, i, len;
// 		var c1, c2, c3;
// 		len = str.length;
// 		i = 0;
// 		out = "";
// 		while ( i < len ) {
// 			c1 = str.charCodeAt( i++ ) & 0xff;
// 			if ( i == len ) {
// 				out += AosSecurityEncode.encodeChars.charAt( c1 >> 2 );
// 				out += AosSecurityEncode.encodeChars.charAt( (c1 & 0x3) << 4 );
// 				out += "==";
// 				break;
// 			}

// 			c2 = str.charCodeAt( i++ );

// 			if ( i == len ) {
// 				out += AosSecurityEncode.encodeChars.charAt( c1 >> 2 );
// 				out +=
// 						AosSecurityEncode.encodeChars.charAt( ((c1 & 0x3) << 4)
// 																| ((c2 & 0xF0) >> 4) );
// 				out += AosSecurityEncode.encodeChars.charAt( (c2 & 0xF) << 2 );
// 				out += "=";
// 				break;
// 			}
// 			c3 = str.charCodeAt( i++ );
// 			out += AosSecurityEncode.encodeChars.charAt( c1 >> 2 );
// 			out += AosSecurityEncode.encodeChars.charAt( ((c1 & 0x3) << 4) | ((c2 & 0xF0) >> 4) );
// 			out += AosSecurityEncode.encodeChars.charAt( ((c2 & 0xF) << 2) | ((c3 & 0xC0) >> 6) );
// 			out += AosSecurityEncode.encodeChars.charAt( c3 & 0x3F );
// 		}
// 		return out;
// 	},
// 	aosDecode : function(str) {
// 		var c1, c2, c3, c4;
// 		var i, len, out;
// 		len = str.length;
// 		i = 0;
// 		out = "";
// 		while ( i < len ) {
// 			do {
// 				c1 = AosSecurityEncode.decodeChars[str.charCodeAt( i++ ) & 0xff];
// 			} while ( i < len && c1 == -1 );

// 			if ( c1 == -1 )
// 				break;

// 			do {
// 				c2 = AosSecurityEncode.decodeChars[str.charCodeAt( i++ ) & 0xff];
// 			} while ( i < len && c2 == -1 );

// 			if ( c2 == -1 )
// 				break;

// 			out += String.fromCharCode( (c1 << 2) | ((c2 & 0x30) >> 4) );

// 			do {
// 				c3 = str.charCodeAt( i++ ) & 0xff;

// 				if ( c3 == 61 )
// 					return out;

// 				c3 = AosSecurityEncode.decodeChars[c3];
// 			} while ( i < len && c3 == -1 );

// 			if ( c3 == -1 )
// 				break;

// 			out += String.fromCharCode( ((c2 & 0XF) << 4) | ((c3 & 0x3C) >> 2) );

// 			do {
// 				c4 = str.charCodeAt( i++ ) & 0xff;

// 				if ( c4 == 61 )
// 					return out;

// 				c4 = AosSecurityEncode.decodeChars[c4];
// 			} while ( i < len && c4 == -1 );

// 			if ( c4 == -1 )
// 				break;

// 			out += String.fromCharCode( ((c3 & 0x03) << 6) | c4 );
// 		}

// 		return out;
// 	}

// } );