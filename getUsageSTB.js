var Promise = require('bluebird'),
	request = require('request');

var App = {
	hit : function(cookie, data) {
		return new Promise(function(resolve, reject) {
			request({
				method : 'POST',
				headers : {
					"Accept" : "application/json, text/javascript, */*; q=0.01",
					"Accept-Encoding" : "gzip, deflate",
					"Accept-Language" : "en-US,en;q=0.8",
					"Content-Type" : "application/x-www-form-urlencoded",
					"Upgrade-Insecure-Requests" : 1,
					"User-Agent" : "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36",
					"X-Requested-With" : "XMLHttpRequest"

				},
				followAllRedirects : true,
				maxRedirects : 10,
				followRedirect : true,
				url : 'http://118.97.167.60:21180/aos-web/ServletAgency?',
				jar : cookie,
				form : {
					commandcode : 100171,
					value : data.no_inet,
					loginname:"admin",
					version :"1.0",
					querytype : 0,
					groupsview : "",
					random :data.time,
					grouplevel:-1
				}

			}, function(err, res, body) {
				if(err)reject(JSON.stringify(err));
				
				resolve(body);
			})
			// resolve("boby");
		})
	}
}

module.exports = App;