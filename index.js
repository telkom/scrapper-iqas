var request = require('request'),
	fs		= require('fs'),
	sugar	= require('sugar'),
	cheerio = require('cheerio'),
	Promise = require('bluebird'),
	_		= require('underscore');

var isJSON = function(str){
	try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}

var noInet = process.argv[2];
var startDate = process.argv[3];
var endDate = process.argv[4];

var CookieParser = function(cookie){
	var result = {};
	cookie = JSON.parse(cookie);
	if(typeof cookie == "object"){
		var result2 = cookie['_jar']['cookies'][0];
		var stringCookie = result2['key']+"="+result2['value'];
		result = stringCookie;
	}

	return result;
}

var readCookie = function(){
	return new Promise(function(resolve, reject){
		fs.readFile('../scrapper-iqas/cookie.txt', function(err, data){
			if(err)reject(err);
			resolve(data.toString());
		})
	})
}

var getUsageSTB = function(cookie){
	return new Promise(function(resolve, reject){
		var data = {
			no_inet : noInet,
			time : 1482887693086,
			startDate : startDate,
			endDate : endDate
		}

		request({
			method : 'POST',
			headers : {
				"Accept" : "application/json, text/javascript, */*; q=0.01",
				"Accept-Encoding" : "gzip, deflate",
				"Accept-Language" : "en-US,en;q=0.8",
				"Content-Type" : "application/x-www-form-urlencoded",
				"Upgrade-Insecure-Requests" : 1,
				"User-Agent" : "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36",
				"X-Requested-With" : "XMLHttpRequest",
				Cookie : cookie

			},
			followAllRedirects : true,
			maxRedirects : 10,
			followRedirect : true,
			url : 'http://118.97.167.60:21180/aos-web/ServletAgency?',
			form : {
				commandcode : 100171,
				value : data.no_inet,
				loginname:"admin",
				version :"1.0",
				querytype : 0,
				groupsview : "",
				random :data.time,
				grouplevel:-1
			}

		}, function(err, res, body) {
			if(isJSON(body)){
				var extractData = JSON.parse(body);
				var stbID = extractData['dataResult_segment'][0].deviceid;

				request({
					method : "POST",
					headers : {
						"Accept" : "application/json, text/javascript, */*; q=0.01",
						"Accept-Encoding" : "gzip, deflate",
						"Accept-Language" : "en-US,en;q=0.8",
						"Content-Type" : "application/x-www-form-urlencoded",
						"Upgrade-Insecure-Requests" : 1,
						"User-Agent" : "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36",
						"X-Requested-With" : "XMLHttpRequest",
						Cookie : cookie

					},
					maxRedirects : 10,
					followRedirect : true,
					url : 'http://118.97.167.60:21180/aos-web/ServletAgency?',
					form : {
						commandcode:100171,
						loginname:"admin",
						querytype:1,
						groupsview: "",
						random:1482900202878,
						grouplevel:-1,
						sessionid:1482900202876,
						pageNo:1,
						pageSize:2000,
						stbid:stbID,
						endtime: data.endDate,
						userid: data.no_inet,
						begintime: data.startDate,
						version:"1.0"
					}
				}, function(err, res, body){
					resolve(body);
				})
			} else {
				resolve(body);
			}
		});
	})
}

var login = function(){
	var cookie2 = request.jar();
	return new Promise(function(resolve,reject){
		request({
			method : 'POST',
			headers : {
				"Accept" : "application/json, text/javascript, */*; q=0.01",
				"Accept-Encoding" : "gzip, deflate",
				"Accept-Language" : "en-US,en;q=0.8",
				"Content-Type" : "application/x-www-form-urlencoded",
				"Upgrade-Insecure-Requests" : 1,
				"User-Agent" : "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36",
				"X-Requested-With" : "XMLHttpRequest"
			},
			followAllRedirects : true,
			maxRedirects : 10,
			jar : cookie2,
			followRedirect : true,
			url : 'http://118.97.167.60:21180/aos-web/res/aos-common/login',
			form : {
				username : "aribi",
				password : "ODQwMTY4",
				isEncode : true
			}

		}, function(err, res, body) {
			if(err)rej(err);
			var cookieString = JSON.stringify(cookie2);
			var parseCookie = CookieParser(cookieString);

			fs.writeFile('../scrapper-iqas/cookie.txt', parseCookie, function(err){
				resolve();
			})
		})

	})
}

var App = function() {
	return new Promise (function(res, rej){
		readCookie()
		.then(function(cookie){
			getUsageSTB(cookie)
			.then(function(resUsage){
				if(!isJSON(resUsage)){
					login()
					.then(function(){
						readCookie()
						.then(function(cookie2){
							getUsageSTB(cookie2)
							.then(function(resUsage2){
								console.log(resUsage2);
							})
						})
					})
				} else {
					console.log(resUsage);
				}
			})
		})
		.catch(function(err){
			console.log(err);
		})
	})
}

App();